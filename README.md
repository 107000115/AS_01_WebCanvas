# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| DynamicWidth                                  | 1~5%     | Y         |


---

### How to use 

Below is the screenshot of my website with 3 parts in the web.

![Website ScreenShot](./ImgMD/img1.png)

The canvas part is on the left-hand side, and the setting part and the function part are on the right-hand side.  
I am going to introduce how I designed each part.

![Website ScreenShot with Names of the Parts](./ImgMD/img2.png)
***

In the **Setting** part, user can change the color of the pen, shapes or text, and the thickness of pen, eraser and shape border can also be adjusted.  
The range of available thickness is from 1px to 20px.  
There are also the font option for text. Both the font-family and font size can be adjusted here.   

<!-- ![Setting Part](./ImgMD/img3.png) -->
<img src="ImgMD/img3.png" alt="Setting Part" width="50%" style="display:block; margin:auto;">

***
  
In the **Function** part, user selects the mode to draw on the canvas.  

For **Pen** and **Eraser** buttons, user clicks on the button then can draw on the canvas directly.  

However, when the **Text** button is clicked on, user has to click again inside the canvas where user want to put the text. The text input box will appear right at the place just clicked. Type the words in the input text box and press the "Enter" key to finish the input.  

The second row contains the shapes option for user, and there are 4 buttons, **Line*, **Circle*, **Triangle** and **Rectangle*.  
To use these modes, user clicks on the button, then clicks on the canvas and drags to make the shape.  
For **Line**, **Triangle** and **Rectangle** mode, the mouse down position is at the left top corner of the shape, while it is the center of circle for **Circle** mode.

There is a checkbox "Filled" in the next row. When it is checked, the shape made will be filled with the same color as the shape border. Otherwise, it will not be filled but only the border, whose width is set in the **Thickness** below.

Furthermore, the **Undo** & **Redo** function are implemented, and the buttons are on the third row. 

The **Reset** button is for the user to clear all the canvas. But don't worry about the accidental touch, because you can simply click on the **Undo** button to get your drawing back.  

Last but not least, the **Save** and the **Upload** function are also implemented. User can save (download) their drawing to the png file with the transparent background, without saying goodbye to their masterpieces. User can upload the drawing to continue editing it, and they can also upload the picture as the background to work on.


<!-- ![Function Part](./ImgMD/img4.png)  -->
<img src="ImgMD/img4.png" alt="Function Part" width="50%" style="display:block; margin:auto;">

***

### Function description

 #### Text Mode  

When implementing **Text** mode, I tried to improve the UI, so I add some code to make users get into the text input box automatically.  

    object.focus();

Due to the above code, user has no need to click the input box before typing words.

Moreover, since the position text pasted at is a little bit different from where the input box appears, I changed the the argument **Y coordinate** of fillText() function from **lastY** to **lastY + fontsize**. However, the varieble **fontsize** is string type, so I have to use parseInt() function to convert the string into number. Below is the final result.  

    ctx.fillText(word, lastX, lastY+ parseInt(fontsize));
    ("word" is the string that user types in the input box)

***  

#### Additional Function - DynamicWidth

I implemented a function called **DinamicWidth**. This is the function that the user can use pen to draw with dynamic width.
The width is in the range of [1:10], and **ctx.lineWidth** will change if the **Dynamic** checkbox is checked.  

This is how I implement the function.

    let direction = false;
        
    function dynamicWidth(){
        if (ctx.lineWidth >=  10 || ctx.lineWidth <= 1) 
            direction = !direction;

        if (direction) 
            ctx.lineWidth++;
        else 
            ctx.lineWidth--;
    }

 ***  

### Gitlab page link

<https://107000115.gitlab.io/AS_01_WebCanvas>


<style>
table th{
    width: 100%;
}
</style>