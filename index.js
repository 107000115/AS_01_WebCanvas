const canvas = document.getElementById('myCanvas');
const ctx = canvas.getContext('2d');
var mode = "Pen";
var snapshot, radius, 
    tfont = "Arial", 
    tsize = 12;
var img = new Image();

var cPushArray = new Array();
var cStep = -1;
var w = 1;

// Initialize
ctx.strokeStyle = 'black';  // 筆觸顏色
ctx.lineJoin = 'round';  // 兩條線交匯處產生 "圓形" 邊角
ctx.lineCap = 'round';  // 筆觸預設為 "圓形"
ctx.lineWidth = 1;  // 筆頭寬度
ctx.font = "12px Arial";

let isDrawing = false;  // 是否允許繪製  (或說是否是 mousedown 下筆狀態)

/* 繪製時的起點座標 */
let lastX = 0;
let lastY = 0;

// window.addEventListener('mousedown', (e) => {
//     document.getElementById("inp").style.display = "none";
// });

/*========== Events Binding ==========*/
canvas.addEventListener('mouseup', (e) => {
    isDrawing = false;
    if (mode == "Text") callTextBox(e);
    else Push();
});
canvas.addEventListener('mouseout', () => isDrawing = false);
canvas.addEventListener('mousedown', (e) => {
    isDrawing = true; // 允許繪製
    takeSnapshot();
    [lastX, lastY] = [e.offsetX, e.offsetY]; // 設定起始點
});

canvas.addEventListener('mousemove', draw);
 

/*========== 繪製函數；在 mousemove 的時候使用 ==========*/
function draw(e) {

    if(!isDrawing) return;  // 沒有允許繪製即退出

    switch (mode) {
        case "Pen":
            ctx.globalCompositeOperation = "source-over";
            /* 繪製路線 Setting */
            ctx.beginPath();  // 開始路徑 or Reset
            ctx.moveTo(lastX, lastY);  // 設定起點
            ctx.lineTo(e.offsetX, e.offsetY);  // 設定終點
            ctx.lineJoin = 'round';  // 兩條線交匯處產生 "圓形" 邊角
            
            if (document.getElementById("dynamic").checked)
                dynamicWidth();     // dynamic width
            else ctx.lineWidth = w;

            ctx.stroke();  // 依照設定開始繪製
            [lastX, lastY] = [e.offsetX, e.offsetY];  // 位置更新
        break;

        case "Eraser":
            ctx.globalCompositeOperation = "destination-out";
            ctx.beginPath();  // 開始路徑 or Reset
            ctx.moveTo(lastX, lastY);  // 設定起點
            ctx.lineTo(e.offsetX, e.offsetY);  // 設定終點
            ctx.lineJoin = 'round';  // 兩條線交匯處產生 "圓形" 邊角
            ctx.stroke();  // 依照設定開始繪製
            [lastX, lastY] = [e.offsetX, e.offsetY];  // 位置更新
        break;

        case "Rectangle":
            restoreSnapshot();
            ctx.globalCompositeOperation = "source-over";
            ctx.beginPath();  // 開始路徑 or Reset
            ctx.rect(lastX,lastY,e.offsetX-lastX,e.offsetY-lastY);
        
            ctx.lineJoin = 'miter';  // 兩條線交匯處產生 "方形" 邊角
            if (document.getElementById("filled").checked) {
                ctx.fillStyle = ctx.strokeStyle;
                ctx.fill();
            }
            ctx.stroke();
        break;

        case "Circle":
            restoreSnapshot();
            radius = Math.sqrt(Math.pow(e.offsetX-lastX, 2)+Math.pow(e.offsetY-lastY, 2));
            ctx.globalCompositeOperation = "source-over";
            ctx.beginPath();  // 開始路徑 or Reset
            ctx.arc(lastX, lastY, radius, 0, 2 * Math.PI);
            ctx.lineJoin = 'round';  // 兩條線交匯處產生 "圓形" 邊角
            if (document.getElementById("filled").checked) {
                ctx.fillStyle = ctx.strokeStyle;
                ctx.fill();
            }
            ctx.stroke();
        break;

        case "Triangle":
            restoreSnapshot();
            ctx.globalCompositeOperation = "source-over";
            ctx.beginPath();  // 開始路徑 or Reset
            ctx.moveTo((lastX+e.offsetX)/2,lastY);
            ctx.lineTo(e.offsetX, e.offsetY);
            ctx.lineTo(lastX, e.offsetY);
            ctx.closePath();
            ctx.lineJoin = 'miter';  // 兩條線交匯處產生 "方形" 邊角
            if (document.getElementById("filled").checked) {
                ctx.fillStyle = ctx.strokeStyle;
                ctx.fill();
            }
            ctx.stroke();
        break;

        case "Line":
            restoreSnapshot();
            ctx.globalCompositeOperation = "source-over";
            ctx.beginPath();  // 開始路徑 or Reset
            ctx.moveTo(lastX, lastY);  // 設定起點
            ctx.lineTo(e.offsetX, e.offsetY);  // 設定終點
            ctx.lineJoin = 'round';  // 兩條線交匯處產生 "圓形" 邊角
            ctx.stroke();  // 依照設定開始繪製
        break;

        default:
            return;
        break;
    }

    // 自動變色功能
    // rainbow();
}

function callTextBox(e) {
    var rect = canvas.getBoundingClientRect();
    let i = document.getElementById("inp");
    i.value = "";
    i.style.height = tsize + 'px';
    i.style.fontSize = tsize + 'px';
    i.style.fontFamily = tfont;
    i.style.color = ctx.strokeStyle;
    
    i.style.left = e.pageX + 'px';
    i.style.top = e.pageY + 'px';
    // i.top  = rect.top + e.offsetY + 'px';
    // console.log(rect.left+e.offsetX, rect.top+e.offsetY);
    // console.log(i.left, i.top);
    i.style.display = "unset";
    i.focus();
}

function addText(e) {
    // document.getElementById("inp").autofocus = false;
    var word = document.getElementById("inp").value;
    ctx.fillStyle = ctx.strokeStyle;
    // console.log(tsize);
    ctx.fillText(word, lastX, lastY+ parseInt(tsize));
    document.getElementById("inp").style.display = "none";
    Push();
}

function change_mode(m) {
    let c = document.getElementById("myCanvas").style;
    switch (m) {
        case "Pen": c.cursor = "url(image/pen.png), default"; break;
        case "Eraser": c.cursor = "url(image/eraser.jpg), default"; break;
        case "Circle": c.cursor = "url(image/circle.png), default"; break;
        case "Line": c.cursor = "url(image/line.png), default"; break;
        case "Rectangle": c.cursor = "url(image/rectangle.png), default"; break;
        case "Triangle": c.cursor = "url(image/triangle.png), default"; break;
        case "Text": c.cursor = "text"; break;
        
    }
    // document.getElementById("test").innerText = c.cursor;
    document.getElementById("mode").innerText = "Mode : " + m;
    mode = m;
}

function change_color(v) {
    document.getElementById("color").innerText = v;
    ctx.strokeStyle = v;
}
function change_thickness(v) {
    document.getElementById("thickness").innerText = v+" px";
    ctx.lineWidth = v;
    w = v;
}
function change_textsize(v) {
    tsize = v.value;
    ctx.font = tsize + 'px ' + tfont;
}
function change_textfont(v) {
    tfont = v.value;
    ctx.font = tsize + 'px ' + tfont;
}

function takeSnapshot() {
    snapshot = ctx.getImageData(0, 0, canvas.width, canvas.height);
}

function restoreSnapshot() {
    ctx.putImageData(snapshot, 0, 0);
}

//redo & undo

function Push() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('myCanvas').toDataURL());
    // console.log(cStep);
    // console.log(cPushArray);
}
function Undo() {
    if (cStep > 0) {
        console.log('undo');
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { 
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvasPic, 0, 0); 
        }
    } else if (cStep == 0) {
        cStep--;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
}
function Redo() {
    console.log('redo');
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { 
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvasPic, 0, 0); 
        }
    }
}

//save & upload

function save() {
    let imgUrl = canvas.toDataURL("image/png");
    let saveA = document.createElement("a");
    document.body.appendChild(saveA);
    saveA.href = imgUrl;
    var Today = new Date;
    saveA.download = "webcanvas_" + Today.getFullYear() + '_' + (Today.getMonth()+1) + '_' + 
        Today.getDate() + '_' + Today.getHours() + '_' + Today.getMinutes() + '_' + Today.getSeconds() ;
    saveA.target = "_blank";
    saveA.click();
}

function upload(e) {
    img.onload = function () {
        ctx.drawImage(img, 0, 0);
    }
    // img.src = e.target.result;
    img.src = URL.createObjectURL(e.files[0]);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(img, 0, 0);
}

function clean() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    Push();
}
/*---------- Dynamic width ----------*/

let direction = false;
        
function dynamicWidth(){
    if(ctx.lineWidth>10) ctx.lineWidth = 1;
    // console.log(ctx.lineWidth);
    if (ctx.lineWidth >=  10 || ctx.lineWidth <= 1) {
        direction = !direction;
    }

    if (direction) {
        ctx.lineWidth++;
    } else {
        ctx.lineWidth--;
    }
}


/*---------- Rainbow 功能 ----------*/
// let hue = 0; // 色相環度數從 0 開始 (的異世界!? XD)

// function rainbow(){
//     // 透過 context 的 strokeStyle 設定筆畫顏色
//     ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
    
//     hue++; // 色相環 度數更新
//     if (hue >= 360) {
//         hue = 0;
//     }
// }
